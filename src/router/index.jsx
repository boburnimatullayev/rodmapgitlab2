import { observer } from 'mobx-react-lite';
import { Navigate, Route, Routes } from 'react-router-dom';
import AuthLayout from '../layouts/AuthLayout';
import MainLayout from '../layouts/MainLayout';
import Login from '../modules/Login';
import authStore from '../store/auth.store';
import Department from '../modules/Department';
import Cards from 'modules/Department/components/Cards';
import List from 'modules/Users/List';
import Roles from 'modules/Roles/List';
import Designation from 'modules/Designation/List';
import UsersCards from 'modules/Users/components/Cards';
import RolesCards from 'modules/Roles/components/Cards';
import DesignationsCards from 'modules/Designation/components/Cards';
import DepartmentDetail from 'modules/Department/Detail';
import RoleDetail from 'modules/Roles/Detail';
import DesignationDetail from 'modules/Designation/Detail';
import UserDetail from 'modules/Users/Detail';
import Roadmaps from 'modules/Roadmap/List';
import RoadmapCards from 'modules/Roadmap/components/Cards';
import RoadmapDetail from 'modules/Roadmap/Detail';

const Router = () => {
	const { isAuth } = authStore;

	if (!isAuth)
	  return (
	    <Routes>
	      <Route path="/" element={<AuthLayout />}>
	        <Route index element={<Navigate to="/login " />} />
	        <Route path="login" element={<Login />} />
	        <Route path="*" element={<Navigate to="/login" />} />
	      </Route>
	      <Route path="*" element={<Navigate to="/login" />} />
	    </Routes>
	  );

	return (
		<Routes>
			<Route path="/" element={<MainLayout />}>
				<Route index element={<Navigate to="/department" />} />

				<Route path="/department" element={<Department />} />
				<Route path="department/create" element={<Cards />} />
				<Route path="department/:id" element={<DepartmentDetail />} />
				<Route path="user" element={<List />} />
				<Route path="user/create" element={<UsersCards />} />
				<Route path="user/:id" element={<UserDetail />} />
				<Route path="role" element={<Roles />} />
				<Route path="role/create" element={<RolesCards />} />
				<Route path="role/:id" element={<RoleDetail />} />
				<Route path="designation" element={<Designation />} />
				<Route path="designation/create" element={<DesignationsCards />} />
				<Route path="designations/:id" element={<DesignationDetail />} />
				<Route path="roadmap" element={<Roadmaps />} />
				<Route path="/roadmap/create" element={<RoadmapCards />} />
				<Route path="roadmap/:id" element={<RoadmapDetail />} />

				<Route path="*" element={<Navigate to="/department" />} />
			</Route>

			<Route path="*" element={<Navigate to="/department" />} />
		</Routes>
	);
};

export default observer(Router);
