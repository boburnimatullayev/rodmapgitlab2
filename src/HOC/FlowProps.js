import { createElement } from 'react';

const FlowProps = (WrappedComponent, additionalProps = {}) => {
	return (props) => {
		return createElement(WrappedComponent, {
			...props,
			...additionalProps,
		});
	};
};

export default FlowProps;
