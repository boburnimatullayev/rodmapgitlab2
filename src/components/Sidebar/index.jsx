import { Box, Flex, Icon, IconButton, Text } from '@chakra-ui/react';
import styles from './index.module.scss';

import clsx from 'clsx';
import { useLocation, useNavigate } from 'react-router-dom';
import { AiFillLeftCircle } from 'react-icons/ai';
import authStore from 'store/auth.store';

import logo from '../../assets/imgs/Frame.jpg';
import { useState } from 'react';
import { ArrowLeftIcon, ArrowRightIcon } from '@chakra-ui/icons';
import { MdLogout } from 'react-icons/md';

const Sidebar = ({ elements }) => {
	const [open, setOpen] = useState(false);

	const navigate = useNavigate();
	const { pathname } = useLocation();

	const onRowClick = (element) => {
		navigate(element.link);
	};

	return (
		<Box className={open ? styles.sidebar_close : styles.sidebar}>
			  <Box className={styles.header}>
				<Flex alignItems="center">
					<img src={logo} alt="logo" />
				</Flex>

				<IconButton
					right={open ? '-3' : 0}
					zIndex={2220}
					pos="absolute"
					variant="unstyled"
					
					onClick={() => setOpen(!open)}
					icon={
						open ? (
							<ArrowRightIcon color="primary.main" />
						) : (
							<ArrowLeftIcon boxSize={4} color="primary.main" />
						)
					}
				/>
			</Box>

			<Box className={styles.body}>
				{elements?.map((element, index) => (
					<Box
						key={index}
						className={clsx(styles.row, {
							[styles.active]: pathname.startsWith(element.link),
						})}
						onClick={() => onRowClick(element)}
					>
						<Box className={styles.element}>
							<Icon as={element.icon} className={styles.icon} />
							<Text className={styles.label}>{element.label}</Text>
						</Box>
					</Box>
				))}
			</Box>

			<Box
				sx={{
					mt: '-42px',
					border: '1px solid #f4f6fa',
					textAlign: 'center',
					p: '8px 12px',	
					cursor: 'pointer',
					fontWeight: 500,
					color: '#9aa6ac',
					fontSize: '16px',
				
				}}
				onClick={() => {
					authStore.logout();
					navigate('/');
				}}
			>
				{open ? <Icon  as={MdLogout}  className={styles.icon2}/> : 'Log Out '}
                
			</Box>
		</Box>
	);
};
export default Sidebar;
