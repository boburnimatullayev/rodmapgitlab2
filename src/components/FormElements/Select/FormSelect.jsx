import { Select } from 'chakra-react-select';
import { Controller } from 'react-hook-form';
import formatGroupLabel from './formatGroupLabel';


const FormSelect = ({
	control,
	name,
	options = [],
	disabled,
	defaultValue = '',
  className,
  validation,
  
	customOnChange = () => {},
	required,
	...props
}) => {

	console.log('deparment',defaultValue);
	return (
		<Controller
			name={name}
			control={control}
			rules={validation}
			render={({ field: { onChange, value },fieldState: { error } }) => (
				<Select
				    errorBorderColor="red.500" isInvalid={error}
					options={options}
                      className={className}
					  defaultInputValue={defaultValue}
					isReadOnly={disabled}
					// components={{ Option: IconOption, SingleValue: CustomSelectValue }}
					value={options.find((option) => option.value === value)}
					onChange={(val) => {
						onChange(val.value);
						customOnChange(val);
					}}
				  chakraStyles={{
						dropdownIndicator: (prev, { selectProps: { menuIsOpen } }) => ({
							...prev,
							'> svg': {
								transitionDuration: 'normal',
								transform: `rotate(${menuIsOpen ? -180 : 0}deg)`
							}
						}),
						
            
					}}
					formatGroupLabel={formatGroupLabel}
					{...props}
					size="lg"
					isRequired={required}
				/>
			)}
		/>
	);
};

export default FormSelect;
