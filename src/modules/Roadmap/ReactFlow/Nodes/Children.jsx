import styles from './style.module.scss';
import { Handle, Position } from 'reactflow';
import { useStore } from 'reactflow';

export default function Children({ setNodes, ...props }) {
	const { id, data } = props;

	const nodesStore = useStore()?.getNodes();

	const Id = nodesStore?.findIndex((item) => item.id === id);

	const onChange = (e) => {
		setNodes((old = []) => {
			old[Id].data.value = e.target.value;
			return [...old];
		});
	};

	return (
		<div className={styles.children}>
			<Handle type="target" id="leftNode" position={Position.Left} />
			<Handle type="source" id="leftNode-2" position={Position.Left} />

			<input onChange={onChange} placeholder="Enter something...." value={data?.value} />
			<Handle type="targer" id="rightNode" position={Position.Right} />
			<Handle type="source" id="rightNode-2" position={Position.Right} />
		</div>
	);
}
