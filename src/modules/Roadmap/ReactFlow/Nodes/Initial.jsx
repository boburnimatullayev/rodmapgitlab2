import styles from './style.module.scss';
import { Handle, Position } from 'reactflow';
import { useDisclosure } from '@chakra-ui/react';
import ModalSidebar from './RightModal';
import { useState } from 'react';

export default function Initial({ setNodes, ...props }) {
	const { id, data } = props;

	const onChange = (e) => {
		setNodes((old = []) => {
			old[0].data.value = e.target.value;
			return [...old];
		});
	};

	return (
		<div className={styles.initial}>
			<input value={data?.value} placeholder="Enter something...." onChange={onChange} />

			<Handle type="target" id="rightNode" position={Position.Bottom} />
			<Handle type="source" id="rightNode-2" position={Position.Bottom} />
		</div>
	);
}
