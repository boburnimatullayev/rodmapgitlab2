import React, { useState } from 'react';
import { Input, Switch, Stack, Heading, Text, Grid, GridItem, Button, IconButton, Flex } from '@chakra-ui/react';
import styles from './style.modal.scss';
import Editor from './ReactQuill';
import { DeleteIcon } from '@chakra-ui/icons';

const ModalSidebar = ({ open, setNodes, nodes, currentItem,setOpen }) => {


	const deletedItem = () => {

		const deletedElement = nodes?.filter((el) => el.id !==  currentItem?.id);
		setNodes(deletedElement);
		setOpen(false);
	};
	const Id = nodes?.findIndex((item) => item.id === currentItem?.id);


	const onChange = (e) => {
		setNodes((old = []) => {
			old[Id].data.value = e.target.value;
			return [...old];
		});
	};

	const handleChange = (e) => {

		if(e !== '<p><br></p>'){
			setNodes((old = []) => {
				old[Id].data.discription = e;
				return [...old];
			});
		}
		
	};

	// const swichOnchange = (event, action) => {
		
	// 	setNodes((old = []) => {
	// 		old[Id].data[action] = currentItem?.data?.[action] === 'target' ? 'source' : 'target';
	// 		return [...old];
	// 	});
	// };

	return (
		<>
			{open ? (
				<>
					<div className="modal-container">
						<div className="modal-content" >
						<Flex justifyContent="space-between" alignItems="center">
						<Heading fontSize="xl" mb={4}>
                              Details
							</Heading>
							<IconButton 
							variant="outline"
			
							border="none"
							  onClick={deletedItem}
							 icon={<DeleteIcon  boxSize={6} color="red" />}
							/>
						</Flex>
							<div className="modal-body">
								<Text mb="8px">Value</Text>
								<Input
									value={currentItem?.data?.value}
									onChange={onChange}
									className={styles.titleInput}
									placeholder="Type title"
									mb={5}
								/>
								<Text mb="8px">Discription</Text>
								<Editor valueDis={currentItem?.data?.discription} handleChange={handleChange} />
								{/* <div className={styles.positions}>
									<Stack align="left" direction="column" gap="25px">
									 <Grid templateColumns='repeat(2, 1fr)' gap={6}>
									 <GridItem>
												<p className={styles.posTitles}>Top</p>
												<Switch
													onChange={(e) => swichOnchange(e, 'top')}
													isChecked={currentItem?.data?.top === 'target' || false}
													size="lg"
												/>{' '}
												{currentItem?.data?.top}
											</GridItem>
											<GridItem>
												<p className={styles.posTitles}>Bottom</p>
												<Switch
													onChange={(e) => swichOnchange(e, 'bottom')}
													isChecked={currentItem?.data?.bottom === 'target' || false}
													size="lg"
												/>
												{currentItem?.data?.bottom}
											</GridItem>
											<GridItem>
												<p className={styles.posTitles}>Left</p>
												<Switch
													onChange={(e) => swichOnchange(e, 'left')}
													isChecked={currentItem?.data?.left === 'target' || false}
													size="lg"
												/>{' '}
												{currentItem?.data?.left}
											</GridItem>
											<GridItem>
												<p className={styles.posTitles}>Right</p>
												<Switch
													onChange={(e) => swichOnchange(e, 'right')}
													isChecked={currentItem?.data?.right === 'target' || false}
													size="lg"
												/>{' '}
												{currentItem?.data?.right}
											</GridItem>
									 </Grid>
									</Stack>
								</div> */}
							</div>
						</div>
						{/* <Button
					 mt={10}
					  colorScheme='red' 
					
					 >
						Deleta node
					 </Button> */}
					</div>
			
				</>
			) : (
				<></>
			)}
		</>
	);
};

export default ModalSidebar;
