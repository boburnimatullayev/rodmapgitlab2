// import Sidebar from "./Sidebar";
import styles from './style.module.scss';
import { Handle, Position } from 'reactflow';
import { useStore } from 'reactflow';

export default function Parent({ setNodes, ...props }) {
	const { id, data } = props;

	const nodesStore = useStore()?.getNodes();

	const Id = nodesStore?.findIndex((item) => item.id === id);

	const onChange = (e) => {
		setNodes((old = []) => {
			old[Id].data.value = e.target.value;
			return [...old];
		});
	};

	return (
		<>
			<div className={styles.parent}>
				<Handle type="target"  id="leftNode" position={Position.Left} />
				<Handle type="source"  id="leftNode-2" position={Position.Left} />
				<Handle type="target"id="topNode" position={Position.Top} />
				<Handle type="source"id="topNode-2" position={Position.Top} />
				<input placeholder="Enter something...." value={data?.value} onChange={onChange} />
				<Handle type="target" id="rightNode" position={Position.Right} />
				<Handle type="source" id="rightNode-2" position={Position.Right} />
				<Handle type="target"  id="bottomNode" position={Position.Bottom} />
				<Handle type="source"  id="bottomNode-2" position={Position.Bottom} />
				

			</div>
		</>
	);
}
