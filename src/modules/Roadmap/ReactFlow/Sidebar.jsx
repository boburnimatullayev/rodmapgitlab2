import FormInput from 'components/FormElements/Input/FormInput';
import Editor from './Nodes/ReactQuill';
import styles from './style.module.scss';

const Sidebar = () => {

	const onDragStart = (event, nodeType) => {
		event.dataTransfer.setData('application/reactflow', nodeType);
		event.dataTransfer.effectAllowed = 'move';
	};

	return (
		<aside className={styles.sidebar}>
			<div className={styles.firstElement} onDragStart={(event) => onDragStart(event, 'parent')} draggable></div>
			<div className={styles.secondElement} onDragStart={(event) => onDragStart(event, 'children')} draggable></div>
		</aside>
	);
};

export default Sidebar;
