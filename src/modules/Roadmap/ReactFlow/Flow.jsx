import { useState, useRef, useCallback, useMemo } from 'react';
import ReactFlow, { ReactFlowProvider, addEdge, } from 'reactflow';
import 'reactflow/dist/style.css';
import Sidebar from './Sidebar';
import styles from './style.module.scss';
import Parent from './Nodes/Parent';
import Children from './Nodes/Children';
import Initial from './Nodes/Initial';
import WithProps from '../../../HOC/FlowProps';
import ModalSidebar from './Nodes/RightModal';
import { useStore } from 'reactflow';



function generateId() {
	return new Date().getTime().toString();
}

const Flow = ({nodes, setNodes, onNodesChange,edges, setEdges, onEdgesChange}) => {
	// const nodesStore = useStore()?.getNodes();

	const reactFlowWrapper = useRef(null);
	// const [nodes, setNodes, onNodesChange] = useNodesState(initialNodes);
	// const [edges, setEdges, onEdgesChange] = useEdgesState([]);
	const [reactFlowInstance, setReactFlowInstance] = useState(null);
	const [menu, setMenu] = useState(null);
	const [currentItem, setCurrentItem] = useState({});
	const [title, setTitle] = useState();
	const [open, setOpen] = useState(false);

	// console.log(`nodes`,nodes);

	const cloneInputType = ({ params }) => {
		const node = nodes.find((item) => item.id === params.target);
		const lineType = node?.type === 'parent';
		return {
			...params,
			animated: !lineType,
			style: { stroke: '#2b77e4', strokeWidth: 2 },
			type: lineType ? 'step' : 'BezierEdge',
		};
	};

	const onConnect = useCallback(
		(params) => setEdges((eds) => addEdge(cloneInputType({ params }), eds)),
		[nodes, edges],
	);

	const onDragOver = useCallback((event) => {
		event.preventDefault();

		event.dataTransfer.dropEffect = 'move';
	}, []);

	const onDrop = useCallback(
		(event) => {
			event.preventDefault();
			const reactFlowBounds = reactFlowWrapper.current.getBoundingClientRect();
			const type = event.dataTransfer.getData('application/reactflow');

			if (typeof type === 'undefined' || !type) {
				return;
			}

			const position = reactFlowInstance.project({
				x: event.clientX - reactFlowBounds.left,
				y: event.clientY - reactFlowBounds.top,
			});
			const positionAbsolute = reactFlowInstance.project({
				x: event.clientX - reactFlowBounds.left,
				y: event.clientY - reactFlowBounds.top,
			});

			const newNode = {
				id: generateId(),
				type,
				position,
				positionAbsolute,
				data: {
					value: '',
					discription: '',
				},
			};

			setNodes((nds) => nds.concat(newNode));
		},
		[reactFlowInstance],
	);

	const nodeTypes = useMemo(
		() => ({
			parent: WithProps(Parent, {
				setNodes,
				setEdges,
				currentItem,
				nodes
		
			}),
			children: WithProps(Children, {
				setNodes,
				setEdges,
				currentItem,
				nodes
				
			}),
			initial: WithProps(Initial, {
				setNodes,
				setEdges,
				currentItem,
				nodes
			}),
		}),
		[setNodes, setEdges, edges,],
	);

	const onPaneClick = useCallback(
		(e) => {
			setMenu(null);
		},
		[setMenu],
	);

	const getCurrentItem = (event, element) => {
		setCurrentItem(element);
	
	};

	const deletedItem = (id) => {

		const deletedElement = nodes?.filter((el) => el.id !== id);
		setNodes(deletedElement);
	};

	const handleclick = (e) => {
		if (
			e.target instanceof HTMLInputElement ||
            e.target.className === '_parent_19fk6_1' ||
            e.target.className === '_parent_19fk6_2'
		) {
			setOpen(true);
		} else {
			setOpen(false);
		}
	};

	return (
		<div className={styles.main}>
			<ReactFlowProvider>
				<div className={styles.flow}>
					<Sidebar />
					<div className={styles.reactFlow} ref={reactFlowWrapper}>
						<ReactFlow
							nodes={nodes}
							edges={edges}
							onNodesChange={onNodesChange}
							onEdgesChange={onEdgesChange}
							onConnect={onConnect}
							onInit={setReactFlowInstance}
							onDrop={onDrop}
							onDragOver={onDragOver}
							nodeTypes={nodeTypes}
							fitView={true}
							positionAbsolute
							onPaneClick={onPaneClick}
							onNodeClick={getCurrentItem}
							onNodesDelete={deletedItem}
							onClick={handleclick}
							zoomOnScroll={false}
							
						/>
					</div>

					<ModalSidebar open={open} setOpen={setOpen} setNodes={setNodes} currentItem={currentItem} nodes={nodes} />
				</div>
			</ReactFlowProvider>
		</div>
	);
};

export default Flow;
