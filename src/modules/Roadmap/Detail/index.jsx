import { Box, Button, Flex, Heading } from '@chakra-ui/react';
import BackButton from 'components/BackButton';
import Header, { HeaderExtraSide, HeaderLeftSide, HeaderTitle } from 'components/Header';
import { Page } from 'components/Page';
import PageCard, { PageCardHeader } from 'components/PageCard';
import useCustomToast from 'hooks/useCustomToast';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import queryClient from 'services/queryClient';
import { useGetRoadmapByIdQuery, useRoadmapCreateMutation, useRoadmapUpdateMutation } from 'services/roadmap.service';
import Flow from 'modules/Roadmap/ReactFlow/Flow';
import { useEffect, useState } from 'react';
import { useNodesState, useEdgesState, handleParentExpand } from 'reactflow';
import FormRow from 'components/FormElements/FormRow';
import FormInput from 'components/FormElements/Input/FormInput';
import FormSelect from 'components/FormElements/Select/FormSelect';
import { useGetAllDepartmentList } from 'services/department.service';

import styles from './Detail.module.scss';
import Textarea from 'components/FormElements/Input/TextArea';
import SimpleLoader from 'components/Loaders/SimpleLoader';
// const initialNodes = [
// 	{
// 		id: '1',
// 		type: 'initial',
// 		position: { x: 250, y: 5 },
// 		data: {
// 			value: '',
// 			top: 'source',
// 			left: 'source',
// 			bottom: 'source',
// 			right: 'source',
// 			discription: '',
// 		},
// 	},
// ];
const RoadmapDetail = () => {

	const navigate = useNavigate();
	const { id } = useParams();
	const [nodes, setNodes, onNodesChange] = useNodesState([]);
	const [edges, setEdges, onEdgesChange] = useEdgesState([]);

	const { successToast } = useCustomToast();

	const { data } = useGetAllDepartmentList({
		params: {
			limit: 10,
			offset: 0,
		},
		tableSlug: '/department',
	});


	const { control, reset, handleSubmit,watch } = useForm({
		defaultValues: {},
	});

	


	const { isLoading } = useGetRoadmapByIdQuery({
		id: id,
		queryParams: {
			cacheTime: false,
			enabled: Boolean(id),
			onSuccess: (res)=>{
				reset({
					...res,
				});		
			},
		},
	});

	const { mutate: updateUser, isLoading: updateLoading } = useRoadmapUpdateMutation({
		
		onSuccess: () => {
			successToast();
			navigate('/roadmap');
		},
	});

	
	useEffect(() => {

		const actionData = watch()?.actions;
		const nodesData  = actionData !== undefined ? JSON.parse(actionData)[0]?.nodes : [];
		const edgesData  = actionData !== undefined ? JSON.parse(actionData)[1]?.edges : [];

		setNodes(nodesData);
		setEdges(edgesData);
	
	},[isLoading]);

	const actions = JSON.stringify([{nodes:nodes},{edges:edges}]);

	const onSubmit = (values) => {

		const updateData = {
			title: values.title,
			description: values.description,
			department_id: values.department_id,
			actions: actions,
			short_link: values.short_link,
			updated_at: 'string',
			id: id,
		};
		updateUser({
			id: id,
			data: updateData
		});
	};

	if (isLoading) return <SimpleLoader h="100vh" />;

	return (
		<form onSubmit={handleSubmit(onSubmit)}>
			<Header>
				<HeaderLeftSide ml={'-40px'}>
					<BackButton />
					<HeaderTitle>Добавить roadmap</HeaderTitle>
				</HeaderLeftSide>
				<HeaderExtraSide>
					<Button type="submit" ml="auto">
            Сохранить
					</Button>
				</HeaderExtraSide>
			</Header>
			<Box borderRadius={'6px'} display={'flex'} flexDirection={'column'} justifyContent={'center'} p={4}>
				<Page>
					<PageCard w="100%">
						<Box  style={{padding:'20px 20px  10px 20px',}}>
							<Flex gap={10}>
								<FormRow label="Заголовок:" required>
									<FormInput
										control={control}
										name="title"
										placeholder="Введите title пользователя"
										autoFocus
										validation={{
											required: {
												value: true,
												message: 'Обязательное поле',
											},
										}}
									/>
								</FormRow>
								<FormRow label="Отделение" required>
									<FormSelect
										options={data?.departments?.map((el) => ({ label: el?.name, value: el?.id }))}
										control={control}
										name="department_id"

										placeholder="Введите отделение"
										validation={{
											required: {
												value: true,
												message: 'Обязательное поле',
											},
										}}
										required={true}
									/>
								</FormRow>
								<FormRow label="Link" required>
									<FormInput
										control={control}
										name="short_link"
										placeholder="Введите short_link"
										autoFocus
										validation={{
											required: {
												value: true,
												message: 'Обязательное поле',
											},
										}}
									/>
								</FormRow>
								<FormRow label="Описание:" required>
									<Textarea
										className={styles.textarea}
										control={control}
										name="description"
										placeholder="Введите oписание"
										required
									/>
								</FormRow>
							</Flex>
						
						</Box>
						<PageCardHeader>
							<HeaderLeftSide>
								<Heading fontSize="xl">Данные o roadmap</Heading>
							</HeaderLeftSide>
						</PageCardHeader>
						<Page>
							<PageCard>
								<Flow
									nodes={nodes}
									setNodes={setNodes}
									onNodesChange={onNodesChange}
									edges={edges}
									setEdges={setEdges}
									onEdgesChange={onEdgesChange}
								/>
							</PageCard>
						</Page>
					</PageCard>
				</Page>
			</Box>
		</form>
	);
};
export default RoadmapDetail;


// const json = "[{\"nodes\":[{\"id\":\"1\",\"type\":\"initial\",\"position\":{\"x\":118.5,\"y\":-62.5},\"data\":{\"value\":\"internet\",\"top\":\"source\",\"left\":\"source\",\"bottom\":\"source\",\"right\":\"source\",\"discription\":\"<p>wedw</p>\"},\"width\":249,\"height\":37,\"selected\":false,\"positionAbsolute\":{\"x\":118.5,\"y\":-62.5},\"dragging\":false},{\"id\":\"1698729227319\",\"type\":\"parent\",\"position\":{\"x\":116.8515625,\"y\":26.96875},\"positionAbsolute\":{\"x\":116.8515625,\"y\":26.96875},\"data\":{\"value\":\"Frontend\",\"discription\":\"<p>wedwe</p>\",\"top\":\"target\",\"left\":\"source\",\"bottom\":\"source\",\"right\":\"source\"},\"width\":253,\"height\":38,\"selected\":false,\"dragging\":false},{\"id\":\"1698729246259\",\"type\":\"parent\",\"position\":{\"x\":-108.15779653597735,\"y\":121.3343271079325},\"positionAbsolute\":{\"x\":-108.15779653597735,\"y\":121.3343271079325},\"data\":{\"value\":\"backend\",\"discription\":\"<p>wedwedwe</p>\",\"top\":\"target\",\"left\":\"source\",\"bottom\":\"source\",\"right\":\"source\"},\"width\":253,\"height\":38,\"selected\":false,\"dragging\":false},{\"id\":\"1698729273183\",\"type\":\"children\",\"position\":{\"x\":194.9640625,\"y\":108.46875},\"positionAbsolute\":{\"x\":194.9640625,\"y\":108.46875},\"data\":{\"value\":\"test2\",\"discription\":\"\",\"top\":\"source\",\"left\":\"target\",\"bottom\":\"source\",\"right\":\"source\"},\"width\":253,\"height\":38,\"selected\":false,\"dragging\":false},{\"id\":\"1698729387262\",\"type\":\"children\",\"position\":{\"x\":192.96406249999995,\"y\":150.96875},\"positionAbsolute\":{\"x\":192.96406249999995,\"y\":150.96875},\"data\":{\"value\":\"test2\",\"discription\":\"\",\"top\":\"source\",\"left\":\"target\",\"bottom\":\"source\",\"right\":\"source\"},\"width\":253,\"height\":38,\"selected\":false,\"dragging\":false},{\"id\":\"1698820874784\",\"type\":\"parent\",\"position\":{\"x\":-105.22210095155037,\"y\":215.49821773162972},\"positionAbsolute\":{\"x\":-105.22210095155037,\"y\":215.49821773162972},\"data\":{\"value\":\"bobur\",\"discription\":\"\",\"top\":\"target\",\"left\":\"source\",\"bottom\":\"source\",\"right\":\"source\"},\"width\":253,\"height\":38,\"selected\":true,\"dragging\":false}]},{\"edges\":[{\"source\":\"1\",\"sourceHandle\":\"rightNode\",\"target\":\"1698729227319\",\"targetHandle\":\"topNode\",\"animated\":false,\"style\":{\"stroke\":\"#2b77e4\",\"strokeWidth\":2},\"type\":\"step\",\"id\":\"reactflow__edge-1rightNode-1698729227319topNode\"},{\"source\":\"1698729227319\",\"sourceHandle\":\"bottomNode\",\"target\":\"1698729246259\",\"targetHandle\":\"topNode\",\"animated\":false,\"style\":{\"stroke\":\"#2b77e4\",\"strokeWidth\":2},\"type\":\"step\",\"id\":\"reactflow__edge-1698729227319bottomNode-1698729246259topNode\"},{\"source\":\"1698729246259\",\"sourceHandle\":\"rightNode\",\"target\":\"1698729273183\",\"targetHandle\":\"leftNode\",\"animated\":true,\"style\":{\"stroke\":\"#2b77e4\",\"strokeWidth\":2},\"type\":\"BezierEdge\",\"id\":\"reactflow__edge-1698729246259rightNode-1698729273183leftNode\"},{\"source\":\"1698729246259\",\"sourceHandle\":\"rightNode\",\"target\":\"1698729387262\",\"targetHandle\":\"leftNode\",\"animated\":true,\"style\":{\"stroke\":\"#2b77e4\",\"strokeWidth\":2},\"type\":\"BezierEdge\",\"id\":\"reactflow__edge-1698729246259rightNode-1698729387262leftNode\"},{\"source\":\"1698729246259\",\"sourceHandle\":\"bottomNode\",\"target\":\"1698820874784\",\"targetHandle\":\"topNode\",\"animated\":false,\"style\":{\"stroke\":\"#2b77e4\",\"strokeWidth\":2},\"type\":\"step\",\"id\":\"reactflow__edge-1698729246259bottomNode-1698820874784topNode\"}]}]"


//  console.log( 'bobur',JSON.parse(json));

