import { Box, Flex } from '@chakra-ui/react';
import Sidebar from '../../components/Sidebar';
import { FiUser } from 'react-icons/fi';
import { BiHomeSmile } from 'react-icons/bi';
import { MdDesignServices } from 'react-icons/md';
import { HiOutlineUsers } from 'react-icons/hi2';
import { Outlet } from 'react-router-dom';

const elements = [
	{
		label: 'Отделение',
		icon: BiHomeSmile,
		link: '/department',
	},
	{
		label: 'Пользователи',
		icon: FiUser,
		link: '/user',
	},
	{
		label: 'Роли',
		icon: HiOutlineUsers,
		link: '/role',
	},
	{
		label: 'Обозначение',
		icon: MdDesignServices,
		link: '/designation',
	},
	{
		label: 'Roadmaps',
		icon: MdDesignServices,
		link: '/roadmap',
	},
];

const MainLayout = () => {
	return (
		<Flex>
			<Box position="sticky" top={0} height="100vh" overflowY="auto">
				<Sidebar elements={elements} />
			</Box>

			<Box background="#f0f0f3" flex={1} height="100vh" width='100%' overflowY="auto">
				<Outlet />
			</Box>
		</Flex>
	);
};
export default MainLayout;
